
Object = require "object"

Object {
	__new: (width, height, tileGenerator) =>
		self.tiles = {}
		self.height = height
		self.width = width

		for x = 1, width
			self.tiles[x] = {}

			for y = 1, height
				self.tiles[x][y] = tileGenerator self, x, y

	print: =>
		for y = 1, @height
			for x = 1, @width
				io.stdout\write (tostring @tiles[x][y]), " "

			io.stdout\write "\n"

		io.stdout\flush!

	update: (tileGenerator) =>
		newTiles = {}

		for x = 1, @width
			newTiles[x] = {}

			for y = 1, @height
				newTiles[x][y] = tileGenerator self, x, y, @tiles[x][y]

		self.tiles = newTiles

		true

	getTile: (x, y) =>
		self.tiles[x] and self.tiles[x][y]

	each: (callback) =>
		for x = 1, @width
			for y = 1, @height
				callback x, y, @tiles[x][y]

	neighbours: (x, y, distance = 1) =>
		neighbours = {}

		for X = x - distance, x + distance
			if X <= 0 or X > @width
				continue

			for Y = y - distance, y + distance
				if Y > 0 and Y <= @height
					table.insert neighbours, self.tiles[X][Y]

		neighbours
}
