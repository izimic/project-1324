
{:each, :reduce, :round} = require "utils"

Object = require "object"

Grid = require "grid"

{:setColor, :rectangle} = love.graphics

Entity = require "entity"
Widget = require "widgets.widget"
Button = require "widgets.button"
Screen = require "widgets.screen"
MapView = require "widgets.map_view"
CraftGridView = require "widgets.craft_grid_view"

generateMap = ->
	grid = Grid 250, 250, (x, y) =>
		if math.sqrt(math.pow(@width / 2 - x, 2) + math.pow(@height / 2 - y, 2)) < 3.5
			"O"
		elseif math.random! < 0.6
			"W"
		else
			" "

	-- Smoothing things.
	for i = 1, 10
		grid\update (x, y, tile) =>
			if x <= 5 or x >= @width - 5 or y <= 5 or y >= @height - 5
				"W"
			else
				tile

		grid\update (x, y, tile) =>
			nearbyWs = reduce (@\neighbours x, y, 2), (value, acc) ->
				acc or= 0

				acc += 1 if value == "W"

				acc

			switch tile
				when " "
					if nearbyWs > 15
						"W"
					else
						tile
				when "W"
					if nearbyWs < 14
						" "
					else
						"W"
				else
					tile

	grid\update (x, y, tile) =>
		if tile == " " and math.random! < 0.1
			"T"
		else if tile == " " and math.random! < 0.1
			"X"
		else
			tile

	for i = 1, 2
		grid\update (x, y, tile) =>
			nearbyWs = reduce (@\neighbours x, y), (value, acc) ->
				acc or= 0

				acc += 1 if value == "W" or value == "S"

				acc

			if tile == " " and nearbyWs > 0
				"S"
			else
				tile

	grid

Inventory = Object {
	__new: (arg = {}) =>
		self.items = {}

	add: (id, amount = 1) =>
		if amount > 1
			for i = 1, amount
				@\push id

		-- We're not using a hash because:
		--   - we may want to keep items sorted at some point
		--   - unique items will have duplicated ids
		--   - we may want to have stack size limits at some point
		for i = 1, #self.items
			item = self.items[i]

			if item.id == id
				item.quantity += 1
				return

		table.insert self.items, {
			id: id
			quantity: amount
		}
}

GameState = require "game_state"

local screen, mapView, gameState, player

love.load = ->
	player = Entity {
		name: "Player Character"
		width: 0.75
		height: 0.75
		inventory: true
	}

	gameState = GameState {
		player: player
	}

	player.x = gameState.map.width / 2
	player.y = gameState.map.height / 2

	gameState\registerEntity Entity {
		name: "Some NPC"
		x: gameState.map.width / 2 + 3.5
		y: gameState.map.width / 2 + 1.5
		onInteract: (entity) =>
			print "Hello there… I guess"
	}

	gameState\registerEntity Entity {
		name: "Alchemic Furnace"
		x: gameState.map.width / 2 + 1.5
		y: gameState.map.width / 2 + 2.5
		onInteract: (entity) =>
			print "interaction requested by #{entity.name}"
	}

	-- FIXME: add cellular automata here to smooth the map

	mapView = MapView {
		camera: player
	}, gameState

	craftGrid = Grid 5, 8, (x, y) -> {}

	craftGridView = CraftGridView {
		width: 32 * 7
		height: 32 * 10
		onUpdate: =>
			@x = screen.width - @width
	}, craftGrid

	screen = Screen {
		debug: true
	}, {
		mapView
		--craftGridView
		Button {
			y: 100
			width: 80
			height: 40
			hidden: true
			label: "new map"
			onMousePressed: =>
				map = GameState.generateMap!
				gameState.map = map
				mapView.map = map
		}
	}

sign = (x) -> x < 0 and -1 or 1

love.update = (dt) ->
	-- FIXME: dt-based updated are going to be hell if network code comes in.
	--        We should use timed gameState frames and do several updates at
	--        a time if required.
	gameState\update dt

	screen\update dt

	mapView.height = screen.height -- - 32 * 2
	mapView.width  = screen.width  -- - 32 * 2

	--mapView.x = 32
	--mapView.y = 32

	speed = 3

	{:isDown} = love.keyboard

	dx = 0
	dy = 0

	-- Yeah sorry, for now it's esdf and not wasd.
	if (isDown "right") or (isDown "f")
		dx += speed * dt
	if (isDown "left")  or (isDown "s")
		dx -= speed * dt
	if (isDown "down")  or (isDown "d")
		dy += speed * dt
	if (isDown "up")    or (isDown "e")
		dy -= speed * dt

	if dx != 0 and dy != 0
		-- FIXME: this assumes abs(dx) = abs(dy), which may not be true on a joystick
		dx = dx * math.sqrt(2) / 2
		dy = dy * math.sqrt(2) / 2

	gameState\moveEntity player, dx, dy

love.draw = ->
	screen\draw!

	love.graphics.print "FPS: " .. love.timer.getFPS!, 0, 0
	love.graphics.print "Camera: [#{mapView.camera.x}:#{mapView.camera.y}]", 0, 16

	each player.inventory.items, (stack, i) ->
		love.graphics.print "[inventory] #{stack.id}",
			0, 16 + i * 16
		love.graphics.print "#{stack.quantity}",
			128, 16 + i * 16

love.mousepressed = (x, y, button) ->
	screen\mousepressed x, y, button

	if button == 2
		if drawable = mapView.focusedDrawable
			return unless drawable.onInteract

			drawable\onInteract player

		return

	-- FIXME: use proper constructor to ensure object structure is valid
	target = mapView\getTileUnderCursor!
	player.currentAction or= {
		timeLeft: 0.33
		onStart: (player, gameState) =>
			@targetAngle = math.atan2 (@target.y - player.y), (@target.x - player.x)
		onUpdate: (player, gameState) =>
			@target =
				x: player.x + 0.9 * math.cos @targetAngle
				y: player.y + 0.9 * math.sin @targetAngle
		onFinished: (player, gameState) =>
			x = math.floor @target.x
			y = math.floor @target.y

			with map = gameState.map
				if .tiles[x] and .tiles[x][y]
					tile = .tiles[x][y]
					switch tile
						when "T"
							player.inventory\add "wood", 1
							.tiles[x][y] = " "
						when "X"
							player.inventory\add "rock", 1
							.tiles[x][y] = " "
		target: target
	}

	-- FIXME: here because no proper action to trigger this callback API ATM
	player.currentAction\onStart player, gameState

