
{:each, :reduce, :round} = require "utils"

Object = require "object"

{:setColor, :rectangle} = love.graphics

Inventory = Object {
	__new: (arg = {}) =>
		self.items = {}

	add: (id, amount = 1) =>
		if amount > 1
			for i = 1, amount
				@\push id

		-- We're not using a hash because:
		--   - we may want to keep items sorted at some point
		--   - unique items will have duplicated ids
		--   - we may want to have stack size limits at some point
		for i = 1, #self.items
			item = self.items[i]

			if item.id == id
				item.quantity += 1
				return

		table.insert self.items, {
			id: id
			quantity: amount
		}
}

Object {
	__new: (arg = {}) =>
		self.x = arg.x or 1
		self.y = arg.y or 1

		self.width = arg.width or 1
		self.height = arg.height or 1

		-- FIXME: This is most unwanted for passive entities like trees
		self.currentAction = arg.currentAction or nil
		self.inventory = arg.inventory or false
		self.inventory = Inventory! if self.inventory == true

		self.name = arg.name or nil
		self.onInteract = arg.onInteract or nil

	draw: (tileX, tileY, tileWidth, mapView) =>
		if mapView.focusedDrawable == self
			setColor 1, 1, 1
		else
			setColor 1, 0, 0

		rectangle "line",
			tileX - tileWidth / 2 -
				tileWidth * @width / 2,
			tileY - tileWidth / 2 -
				tileWidth * @height / 2,
			tileWidth * @width,
			tileWidth * @height

		if action = @currentAction
			rectangle "fill",
				tileX - tileWidth + 8,
				tileY - tileWidth - 16,
				(tileWidth - 16) * (action.timeLeft / action.totalTime), 8
			rectangle "line",
				tileX - tileWidth + 8,
				tileY - tileWidth - 16,
				tileWidth - 16, 8

			setColor 0, 0.5, 0.5

			tileX, tileY = mapView\getTilePosition action.target.x, action.target.y
			rectangle "line",
				tileX - tileWidth,
				tileY - tileWidth,
				tileWidth,
				tileWidth
}


