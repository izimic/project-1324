
{:each, :reduce} = require "utils"

{:setColor, :rectangle} = love.graphics

Widget = require "widgets.widget"

Widget.__extend {
	__new: (arg = {}, grid) =>
		Widget.__new self, arg, children

		self.grid = grid
		self.camera = arg.camera or {
			x: math.floor grid.width / 2
			y: math.floor grid.height / 2
		}

	update: (dt) =>
		Widget.update self, dt

		tileWidth = 32

		cursorX, cursorY = love.mouse.getPosition!

		centerOffsetX = (@width - @grid.width * tileWidth) / 2
		centerOffsetY = (@height - @grid.height * tileWidth) / 2

		tileX = (cursorX - @x - centerOffsetX) / tileWidth + 1
		tileY = (cursorY - @y - centerOffsetY) / tileWidth + 1

		if tileX < 1 or tileX > @grid.width + 1
			@focusedTile = nil
			return
		if tileY < 1 or tileY > @grid.height + 1
			@focusedTile = nil
			return

		@focusedTile = {
			x: math.floor tileX
			y: math.floor tileY
		}

	mousepressed: (x, y, button) =>
		if @focusedTile
			print @focusedTile.x, @focusedTile.y

	draw: =>
		tileWidth = 32

		setColor 0, 0, 0
		rectangle "fill", @x, @y, @width, @height

		setColor 1, 1, 1
		rectangle "line", @x, @y, @width, @height

		cursorX, cursorY = love.mouse.getPosition!

		centerOffsetX = (@width - @grid.width * tileWidth) / 2
		centerOffsetY = (@height - @grid.height * tileWidth) / 2

		for x = 1, @grid.width
			tileX = @x + tileWidth * (x - 1) +
				centerOffsetX

			for y = 1, @grid.height
				tileY = @y + tileWidth * (y - 1) +
					centerOffsetY

				hasFocus = if @focusedTile
					x == @focusedTile.x and
					y == @focusedTile.y

				if hasFocus
					setColor 1, 1, 1
				else
					setColor 0.5, 0.5, 0.5

				rectangle "line", tileX, tileY,
					tileWidth, tileWidth
}
