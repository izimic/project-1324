
{:each} = require "utils"

Object = require "object"

{:setColor, :rectangle} = love.graphics

local Widget
Widget = Object {
	__new: (arg = {}, children = {}) =>
		self.x = arg.x or 0
		self.y = arg.y or 0
		self.width    = arg.width  or 0
		self.height   = arg.height or 0
		self.debug    = arg.debug  or false
		self.hidden   = arg.hidden or false
		self.children = children

		self.onUpdate = arg.onUpdate or (dt) =>

	update: (dt) =>
		@\onUpdate self, dt
		each @children, (x) ->
			return if x.hidden
			x\update dt

	draw: =>
		if @debug
			setColor 255, 0, 0
			rectangle "line", @x, @y, @width, @height

		each @children, (x) ->
			return if x.hidden
			x\draw!

	mousepressed: (x, y, button) =>
		unless x >= @x and x < @x + @width
			return

		unless y >= @y and y < @y + @height
			return

		for child in *@children
			continue if child.hidden

			if Widget.STOP == child\mousepressed x, y, button
				return Widget.STOP

	STOP: "STOP"
}

Widget

