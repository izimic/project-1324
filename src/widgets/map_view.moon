
{:each, :reduce} = require "utils"

{:setColor, :rectangle, :circle} = love.graphics

Widget = require "widgets.widget"

TILES = require "data.tiles"

Widget.__extend {
	__new: (arg = {}, gameState) =>
		Widget.__new self, arg, children

		self.tileWidth = arg.tileWidth or 64

		self.gameState = gameState
		self.map = gameState.map
		self.camera = arg.camera or {
			x: math.floor @map.width / 2
			y: math.floor @map.height / 2
		}

	-- gets on-map coordinates for an object under a
	-- cursor that has on-screen coordinates
	-- NOTE: return coordinates are not rounded, you may
	--       want to do that
	getTileUnderCursor: (cursorX, cursorY) =>
		unless cursorX or cursorY
			cursorX, cursorY = love.mouse.getPosition!

		--centerOffsetX = (@width - @map.width * @tileWidth) / 2 - @camera.x * @tileWidth
		--centerOffsetY = (@height - @map.height * @tileWidth) / 2 - @camera.y * @tileWidth

		centerOffsetX = @width / 2 - @camera.x * @tileWidth
		centerOffsetY = @height / 2 - @camera.y * @tileWidth

		tileX = (cursorX - @x - centerOffsetX) / @tileWidth
		tileY = (cursorY - @y - centerOffsetY) / @tileWidth

		{
			x: tileX
			y: tileY
		}

	-- get on-screen coordinates for an object based on
	-- its on-map coordinates
	getTilePosition: (x, y) =>
		tileX = @x + (@width + @tileWidth) / 2 +
			@tileWidth * (x - @camera.x)
		tileY = @y + (@height + @tileWidth) / 2 +
			@tileWidth * (y - @camera.y)

		tileX, tileY

	draw: =>
		-- FIXME: WTF 3? Why *3*? What sorcery is this?
		--        It clearly doesn't work with lower values,
		--        even though in *theory* 1 should be the
		--        right offset… right?
		horizontalTiles = 3 + math.ceil (@width / @tileWidth)
		verticalTiles = 3 + math.ceil (@height / @tileWidth)

		for i = 1, verticalTiles
			y = math.floor @camera.y + i - verticalTiles / 2 + verticalTiles % 2 / 2 - 1

			if y < 1 or y > @map.height
				continue

			for j = 1, horizontalTiles
				x = math.floor @camera.x + j - horizontalTiles / 2 + horizontalTiles % 2 / 2 - 1

				tileX, tileY = @\getTilePosition x, y

				tile = @map\getTile x, y

				-- most likely out of bounds
				continue unless tile

				TILES[tile]\draw tileX, tileY, @tileWidth

		-- FIXME: Better handle objects and entities so
		--        that drawing can be done in an
		--        homogeneous manner.
		-- FIXME: do NOT display everything, for real
		[[each @gameState.map.objects, (object) ->
			tileX, tileY = @\getTilePosition object.x, object.y

			setColor 0, 0.5, 0.1
			rectangle "fill",
				tileX - @tileWidth / 2 -
					@tileWidth * object.width / 2,
				tileY - @tileWidth / 2 -
					@tileWidth * object.height / 2,
				@tileWidth * object.width,
				@tileWidth * object.height
		]]

		-- TODO: This should go in \update but whatever
		self.focusedDrawable = nil

		cursorPosition = @\getTileUnderCursor!

		each @gameState.drawables, (entity) ->
			-- FIXME: don't display things that are too
			--        far out.
			tileX, tileY = @\getTilePosition entity.x, entity.y

			if entity.name or entity.onInteract
				distance = (a, b) ->
					math.sqrt (
						math.pow(b.y - a.y, 2) +
						math.pow(b.x - a.x, 2)
					)

				-- FIXME: Focus update or something? idk
				if (distance cursorPosition, entity) < 1
					self.focusedDrawable = entity

			entity\draw tileX, tileY, @tileWidth, self

		setColor 1, 1, 1
}
