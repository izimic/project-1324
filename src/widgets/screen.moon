
{:setColor, :rectangle} = love.graphics

Widget = require "widgets.widget"

Widget.__extend {
	__new: (arg = {}, children = {}) =>
		Widget.__new self, arg, children

	update: (dt) =>
		@width, @height = love.graphics.getDimensions!
		@x = 0
		@y = 0

		Widget.update self, dt
}

