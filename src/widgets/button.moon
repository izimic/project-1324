
{:each} = require "utils"

{:setColor, :rectangle} = love.graphics

Widget = require "widgets.widget"

Widget.__extend {
	__new: (arg = {}, children = {}) =>
		Widget.__new self, arg, children

		self.label = arg.label or ""
		self.onMousePressed = arg.onMousePressed or ->

	draw: =>
		setColor 0.25, 0.25, 0.25
		rectangle "fill", @x, @y, @width, @height

		setColor 1, 1, 1
		rectangle "line", @x, @y, @width, @height

		love.graphics.print (@label or "???"), @x, @y

	mousepressed: (x, y, button) =>
		unless x >= @x and x < @x + @width
			return

		unless y >= @y and y < @y + @height
			return

		self.onMousePressed self, x, y, button

}
