
{
	each: (table, callback) ->
		for i = 1, #table
			callback table[i], i

	reduce: (table, functor) ->
		accumulator = nil

		for i = 1, #table
			accumulator = functor table[i], accumulator

		accumulator

	find: (table, functor) ->
		for i = 1, #table
			if functor table[i], i
				return table[i], i

	round: (x) ->
		math.floor x + 0.5
}

