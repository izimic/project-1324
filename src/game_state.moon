
{:each, :find, :reduce, :round} = require "utils"

Object = require "object"
Grid = require "grid"
Map = require "map"
Entity = require "entity"

TILES = require "data.tiles"

Object {
	__new: (arg = {}) =>
		self.player = arg.player
		self.map = arg.tiles or self.generateMap!

		self.entities = {}
		self.drawables = {}

		-- has to be done whenever self.map gets replaced
		-- (also remove the previous objects first)
		each self.map.objects, (mapObject) ->
			table.insert self.drawables, mapObject

		@\registerEntity self.player
		each (arg.entities or {}), (entity) ->
			@\registerEntity entity

	registerEntity: (entity) =>
		table.insert @entities, entity
		table.insert @drawables, entity

	update: (dt) =>
		table.sort @drawables, (a, b) -> a.y < b.y

		each @entities, (entity) ->
			if action = entity.currentAction
				unless action.totalTime
					action.totalTime = action.timeLeft

				action.timeLeft -= dt

				if action.timeLeft <= 0
					(action.onFinished or ->) action, entity, self

					entity.currentAction = nil
				else
					(action.onUpdate or ->) action, entity, self

	collidesIfMoved: (entity, dx, dy) =>
		f = math.floor
		c = math.ceil

		tx = entity.x + dx
		ty = entity.y + dy

		-- Checks for collision with an unwalkable tile.
		for x = f(entity.x + dx - entity.width / 2), c(entity.x + dx - entity.width / 2)
			for y = f(entity.y + dy - entity.height / 2), c(entity.y + dy - entity.height / 2)
				tile = self.map\getTile(x, y)

				if (TILES[tile] or {}).walkable
					continue

				-- collides?
				collides = do
					(x     < tx + entity.width  / 2) and
					(x + 1 > tx - entity.width  / 2) and
					(y     < ty + entity.height / 2) and
					(y + 1 > ty - entity.height / 2)

				if collides
					return true

		-- Checks for collision with on-map objects.
		-- TODO: optimisation
		return true if find @drawables, (object) ->
			return if object == entity

			x = object.x - object.width / 2
			y = object.y - object.height / 2
			ow = object.width
			oh = object.height

			do
				(x      < tx + entity.width  / 2) and
				(x + ow > tx - entity.width  / 2) and
				(y      < ty + entity.height / 2) and
				(y + oh > ty - entity.height / 2)

		false

	-- assumes “entity” is a valid entity present in
	-- the game state.
	moveEntity: (entity, dx, dy) =>
		if @\collidesIfMoved entity, dx, 0
			dx = 0
		if @\collidesIfMoved entity, 0, dy
			dy = 0

		entity.x += dx
		entity.y += dy

	generateMap: ->
		chunks = Grid 5, 5, (x, y) =>
			if x == 3 and y == 3
				return "camp"

			r = math.random!

			if r < 0.1
				"camp"
			elseif r < 0.2
				"ruins"
			elseif r < 0.4
				"desert"
			elseif r < 0.6
				"forest"
			elseif r < 0.8
				"pond"
			else
				"plains"

		map = Map 250, 250, (x, y) =>
			-- hardcoded debug center-of-map thing
			if math.sqrt(math.pow(@width / 2 - x, 2) + math.pow(@height / 2 - y, 2)) < 3.5
				return "O"

			chunkX = 1 + math.floor x / 50
			chunkY = 1 + math.floor y / 50
			if chunkX > 5 or chunkY > 5
				return "W"

			switch chunks.tiles[chunkX][chunkY]
				when "forest"
					"F"
				when "desert"
					if math.random! < 0.6
						"S"
					else
						" "
				when "pond"
					if math.random! < 0.6
						"W"
					else
						" "
				when "camp"
					if math.random! < 0.5
						"S"
					else
						" "
				else
					if math.random! < 0.6
						"W"
					else
						" "

		countNeighbours = (x, y, tile, type, distance = 1) =>
			reduce (@\neighbours x, y, distance), (value, acc) ->
				acc or= 0

				acc += 1 if value == type

				acc

		-- Smoothing things.
		for i = 1, 10
			if false
				map\update (x, y, tile) =>
					if x <= 5 or x >= @width - 5 or y <= 5 or y >= @height - 5
						"W"
					else
						tile

			map\update (x, y, tile) =>
				nearbyWs = countNeighbours map, x, y, tile, "W", 2
				nearbySs = countNeighbours map, x, y, tile, "S", 2
				nearbyFs = countNeighbours map, x, y, tile, "F", 2

				switch tile
					when " "
						if nearbyWs > 14
							"W"
						elseif nearbySs > 17
							"S"
						elseif nearbyFs > 15
							"F"
						else
							tile
					when "W"
						if nearbyWs < 14
							" "
						else
							"W"
					when "S"
						if nearbySs < 12
							" "
						else
							"S"
					when "F"
						if nearbyFs < 15
							" "
						else
							"F"
					else
						tile

		-- ruins/pavement expansion
		map\update (x, y, tile) =>
			nearbyOs = countNeighbours map, x, y, tile, "O", 4

			if tile == " "
				r = math.random!
				if r < 0.1 and nearbyOs > 9
					"O"
				elseif r < 0.2 and nearbyOs > 12
					"O"
				elseif r < 0.3 and nearbyOs > 15
					"O"
				elseif r < 0.5 and nearbyOs > 17
					"O"
				else
					tile
			else
				tile

		-- object generation
		map\update (x, y, tile) =>
			if tile == "F"
				if math.random! < 0.2
					table.insert map.objects, Entity {
						type: "tree"
						x: x + math.random!
						y: y + math.random!
						width: 0.5
						height: 0.5
					}

				return " "
			elseif tile == " "
				if math.random! < 0.02
					table.insert map.objects, Entity {
						type: "tree"
						x: x + math.random!
						y: y + math.random!
						width: 0.5
						height: 0.5
					}

			tile

		map
}

