
(self = {}) ->
	self.__extend or= (newSelf) ->
		newObject = {}

		setmetatable newObject, {
			__call: (_, ...) ->
				instance = {}

				setmetatable instance, {
					__index: (_, key) ->
						newSelf[key] or self[key]
				}

				(newSelf.__new or self.__new) instance, ...

				instance
		}

		newObject

	setmetatable self, {
		__call: (...) =>
			instance = {}

			setmetatable instance, {
				__index: self
			}

			self.__new instance, ...

			instance
	}

